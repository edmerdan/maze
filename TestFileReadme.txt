MazeTestFile = valid solvable maze, has two entrances
MazeTestFile1 = unsolvable maze,has zero entrances
MazeTestFile2 = unsolvable maze,has one entrance
MazeTestFile3 = unsolveable maze, has two entrances but no solveable path
MazeTestFile4 = unsolveable maze, not large enough to be a maze
MazeTestFile5 = valid solvable maze, has 3 entrances
MazeTestFile6 = unsolveable maze, contains a non-zero, non-one character