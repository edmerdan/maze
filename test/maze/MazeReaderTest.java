package maze;

import java.io.IOException;
import maze.exception.InvalidMazeSizeException;
import maze.exception.InvalidMazeTileException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class MazeReaderTest {

    public MazeReaderTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of readMazeFileToArray method, of class MazeReader.
     *
     * @throws java.lang.Exception
     */
    @Test(expected = IOException.class)
    public void testBadFilePath() throws Exception {
        String mazeFile = "BadFilePath.txt"; //file does not exist
        MazeReader.readMazeFileToArray(mazeFile);

    }

    @Test(expected = InvalidMazeSizeException.class)
    public void testFileTooSmall() throws Exception {
        String mazeFile = "MazeTestFile4.txt"; //file not large enough to be a maze
        MazeReader.readMazeFileToArray(mazeFile);

    }
    @Test(expected = InvalidMazeTileException.class)
    public void testBadDataInFile() throws Exception {
        String mazeFile = "MazeTestFile6.txt"; //file not large enough to be a maze
        MazeReader.readMazeFileToArray(mazeFile);

    }

    
}
