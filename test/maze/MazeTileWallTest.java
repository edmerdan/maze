package maze;

import maze.exception.TryingToExploreBadMazeLocationException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class MazeTileWallTest {
    
    public MazeTileWallTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of canExplore method, of class MazeTileWall.
     */
    @Test
    public void testCanExplore() {
        MazeTileWall instance = new MazeTileWall();
        boolean expResult = false;
        boolean result = instance.canExplore();
        assertEquals(expResult, result);

    }

    /**
     * Test of isExplored method, of class MazeTileWall.
     */
    @Test
    public void testIsExplored() {
        MazeTileWall instance = new MazeTileWall();
        boolean expResult = false;
        boolean result = instance.isExplored();
        assertEquals(expResult, result);

    }

    /**
     * Test of shouldExplore method, of class MazeTileWall.
     */
    @Test
    public void testShouldExplore() {
        MazeTileWall instance = new MazeTileWall();
        boolean expResult = false;
        boolean result = instance.shouldExplore();
        assertEquals(expResult, result);

    }

    /**
     * Test of setExplored method, of class MazeTileWall.
     */
    @Test(expected = TryingToExploreBadMazeLocationException.class)
    public void testSetExplored() throws Exception {
        MazeTileWall instance = new MazeTileWall();
        instance.setExplored();

    }


    /**
     * Test of getDrawState method, of class MazeTileWall.
     */
    @Test
    public void testGetDrawState() {
        MazeTileWall instance = new MazeTileWall();
        char expResult = 'W';
        char result = instance.getDrawState();
        assertEquals(expResult, result);

    }
    
}
