package maze;

import maze.exception.NoMazeExitException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class MazeSolverTest {

    public MazeSolverTest() {
    }

    @BeforeClass
    public static void setUpClass() {

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of solve method, of class MazeSolver.
     */
    @Test(expected = NoMazeExitException.class)
    public void testSolver() throws Exception {
        MazeTile[][] unsolvedmaze;
        unsolvedmaze = MazeReader.readMazeFileToArray("MazeTestFile2.txt"); //maze file does not have two exits and is unsolveable
        MazeSolver solver = new MazeSolver(unsolvedmaze);
        solver.solve();
    }




}
