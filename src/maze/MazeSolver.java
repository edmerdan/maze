package maze;

import maze.exception.InvalidMazeTileStateException;
import maze.exception.NoMazeExitException;
import maze.exception.TryingToExploreBadMazeLocationException;

public class MazeSolver {
    private MazeTile[][] maze;
    private int rows;
    private int cols;
    private boolean solvedMaze;

    /**
     * Solves the provided maze by reference
     *
     * @param newMaze The maze to solve by reference
     */
    public MazeSolver(MazeTile[][] newMaze) {
        maze = newMaze;
        solvedMaze = false;
        // Assuming the constructed maze is at least rectangular.
        rows = maze.length - 1;
        cols = maze[0].length - 1;
    }

    /**
     * Solves the maze given to the constructor
     *
     * @throws NoMazeExitException
     * @throws TryingToExploreBadMazeLocationException
     */
    public void solve()
            throws NoMazeExitException, TryingToExploreBadMazeLocationException {
        boolean foundExit = false;
        boolean foundStart = false;
        int[] startPoint = new int[2];

        // Look for open spaces on the edges of the maze to determine where
        // to begin and end. Checks from top left to top right, then descends.
        edgeFinder:
        for (int i = 0; i <= rows; i++) {
            for (int j = 0; j <= cols; j++) {
                // Only bother comparing edges.
                if (i == 0 || i == rows || j == 0 || j == cols) {
                    if (!foundExit && maze[i][j].canExplore()) {
                        // We don't need to care where the exit is until we're
                        // iterating through the maze, so we just set a flag and
                        // ignore it for now.
                        foundExit = true;
                    } else if (foundExit && maze[i][j].canExplore()) {
                        foundStart = true;
                        startPoint[0] = i;
                        startPoint[1] = j;
                        // Don't continue iterating once we find two open spaces
                        // at the edge; it's just a waste of cpu cycles.
                        break edgeFinder;
                    }
                }
            }
        }

        // If we didn't find two open spaces on the edges, the maze is
        // unsolvable. Throw an error.
        if (!foundStart) {
            throw new NoMazeExitException();
        }

        explore(startPoint[1], startPoint[0], true);
    }

    /**
     * Recursively explore and thus solve the maze.
     *
     * @param x The X coordinate of the start position.
     * @param y The Y coordinate of the start position.
     * @param start First iteration cookie.
     * @throws NoMazeExitException
     * @throws TryingToExploreBadMazeLocationException
     */
    public void explore(int x, int y, boolean start)
            throws NoMazeExitException, TryingToExploreBadMazeLocationException {
        // If the location we're exploring is inside the array, our previous tile
        // is not the exit. Explore further.
        if (start || (!solvedMaze && x >= 0 && x <= cols && y >= 0 && y <= rows)) {
            maze[y][x].setExplored();

            // Check new paths: Right, Left, Down, Up.
            // Check if the next place is out of bounds to avoid running a
            // method on an object that doesn't exist. If we're not at the start
            // we know we want to explore out of bounds anyway, since that's
            // our win condition.
            if (x + 1 <= cols) {
                if (maze[y][x + 1].shouldExplore()) {
                    explore(x + 1, y, false);
                }
            } else if (!start) {
                explore(x + 1, y, false);
            }
            if (x - 1 >= 0) {
                if (maze[y][x - 1].shouldExplore()) {
                    explore(x - 1, y, false);
                }
            } else if (!start) {
                explore(x - 1, y, false);
            }
            if (y + 1 <= rows) {
                if (maze[y + 1][x].shouldExplore()) {
                    explore(x, y + 1, false);
                }
            } else if (!start) {
                explore(x, y + 1, false);
            }
            if (y - 1 >= 0) {
                if (maze[y - 1][x].shouldExplore()) {
                    explore(x, y - 1, false);
                }
            } else if (!start) {
                explore(x, y - 1, false);
            }

            // If we're executing this far down, it means either none of the
            // paths branching from here are the correct path which means this
            // also isn't the correct path, or we already found the exit and
            // need to just back out of the recursion without changing flags.
            if (!solvedMaze) {
                maze[y][x].setBadPath();
            }
        } else if (x > cols || x < 0 || y > rows || y < 0) {
            solvedMaze = true;
        }

        // Recursion should have found an exit by now. If there isn't one, turns
        // out the maze has no path to an exit.
        if (start && !solvedMaze) {
            throw new NoMazeExitException();
        }
    }

    // I was using this for testing, figured I may as well leave it here until
    // final version.
    public void printMaze() throws InvalidMazeTileStateException {
        System.out.println(solvedMaze);
        for (int i = 0; i <= rows; i++) {
            for (int j = 0; j <= cols; j++) {
                System.out.print(maze[i][j].getDrawState());
            }
            System.out.print("\n");
        }
    }
}
