package maze.exception;

/**
 * An exit could not be found for the provided maze.
 */
public class NoMazeExitException extends Exception {
    public NoMazeExitException() {
        super();
    }
}
